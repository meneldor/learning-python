#!/usr/bin/env python
import sys

def countLines(filename):
    fo = open(filename)
    return(len(fo.readlines()))

def countChars(filename):
    fo = open(filename)
    return(len(fo.read()))

def test(filename):
    print('Number of lines: ', countLines(filename))
    print('Number of chars: ', countChars(filename))

def main():
    if len(sys.argv) < 2:
        print('Usage: mymod <filename>')
        return
    filename = sys.argv[1]
    test(filename)

if __name__ == '__main__':
    main()
